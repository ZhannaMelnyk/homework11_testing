import CartParser from './CartParser';
import uuid from 'uuid';
import fs from 'fs';
const path = require("path");
const expectedJson = require('../samples/cart.json')
jest.mock('uuid');
jest.mock('fs')

let parser;

beforeEach(() => {
  parser = new CartParser();
});

describe('CartParser - unit tests', () => {
  // Add your unit tests here.

  it('json when call parse method', () => {
    // Arrange
    const
      path = 'path',
      content = `Product name,Price,Quantity
                 Mollis consequat,9.00,2`,
      id = 'id',
      expectedResult = {
        items: [{
          id: "id",
          name: "Mollis consequat",
          price: 9,
          quantity: 2
        }],
        total: 18
      }

    uuid.mockImplementation(() => id);
    fs.readFileSync.mockImplementation(() => content);

    // Act
    const result = parser.parse(path);

    // Assert
    expect(result).toEqual(expectedResult)
  })

  it('array with error when call parse method with not valid content', () => {
    // Arrange
    const
      path = 'path',
      content = `Product,Price,Quantity
                 Mollis consequat,9.00,2`,
      id = 'id'

    uuid.mockImplementation(() => id);
    fs.readFileSync.mockImplementation(() => content);

    // Assert
    expect(() => parser.parse(path)).toThrow('Validation failed!')
  })

  it('correct parameters when calling readFile method', () => {
    // Arrange
    const path = 'path',
      encoding = 'utf-8'

    // Act
    parser.readFile(path);

    // Assert
    expect(fs.readFileSync).toHaveBeenCalledWith(path, encoding, 'r')
  })

  it('array with one error when header name is incorrect', () => {
    // Arrange
    const content = 'Product,Price,Quantity';

    const expectedResult = [{
      column: 0,
      message: 'Expected header to be named \"Product name\" but received Product.',
      row: 0,
      type: 'header'
    }]

    // Act
    const result = parser.validate(content);

    // Assert
    expect(result).toEqual(expectedResult)
  })

  it('array with one error when received amount of cells is incorrect', () => {
    // Arrange
    const content = `Product name,Price,Quantity
    Mollis consequat
    Mollis consequat,9.00,2`;

    const expectedResult = [{
      column: -1,
      message: "Expected row to have 3 cells but received 1.",
      row: 1,
      type: 'row'
    }]

    // Act
    const result = parser.validate(content);

    // Assert
    expect(result).toEqual(expectedResult)
  })

  it('array with one error when string type cell in body line is empty', () => {
    // Arrange
    const content = `Product name,Price,Quantity
    ,9.00,2`;

    const expectedResult = [{
      column: 0,
      message: "Expected cell to be a nonempty string but received \"\".",
      row: 1,
      type: 'cell'
    }]

    // Act
    const result = parser.validate(content);

    // Assert
    expect(result).toEqual(expectedResult)
  })

  it('array with one error when number type cell in body line is not valid', () => {
    // Arrange
    const content = `Product name,Price,Quantity
    Mollis consequat,9.00,-2`;

    const expectedResult = [{
      column: 2,
      message: "Expected cell to be a positive number but received \"-2\".",
      row: 1,
      type: 'cell'
    }]

    // Act
    const result = parser.validate(content);

    // Assert
    expect(result).toEqual(expectedResult)
  })

  it('item object when call parseLine method', () => {
    // Arrange
    const
      id = 'id',
      name = 'Mollis consequat',
      price = 9,
      quantity = 2

    uuid.mockImplementation(() => id);

    const csvLine = `${name},${price}.00,${quantity}`;

    const expectedItem = {
      id,
      name,
      price,
      quantity
    };

    // Act
    const item = parser.parseLine(csvLine);

    // Assert
    expect(item).toEqual(expectedItem);
  })

  it('total price when items have price and quantity', () => {
    // Arrange
    const firstItem = {
      price: 9,
      quantity: 2
    }
    const secondItem = {
      price: 10.32,
      quantity: 1
    }
    const items = [firstItem, secondItem];
    const expectedTotal = 28.32;

    // Act
    const totalPrice = parser.calcTotal(items);

    // Assert
    expect(totalPrice).toBe(expectedTotal)
  })

  it('error object when call createError message', () => {
    // Arrange
    const
      type = 'header',
      row = 0,
      column = 1,
      message = `Expected header to be named "Product name" but received Price.`;

    const expectedError = {
      'type': 'header',
      'row': 0,
      'column': 1,
      'message': `Expected header to be named "Product name" but received Price.`
    }

    // Act 
    const error = parser.createError(type, row, column, message);

    // Assert
    expect(error).toEqual(expectedError)
  })
});


describe('CartParser - integration test', () => {
  // Add your integration test here.
  it('return json when parse content from CSV file', () => {
    // Arrange
    uuid.v4.mockImplementation(require.requireActual('uuid').v4);
    fs.readFileSync.mockImplementation(require.requireActual('fs').readFileSync);

    // Act
    const parsedJson = parser.parse(path.resolve(__dirname, "../samples/cart.csv"));

    const expectedItems = expectedJson.items.map(item => {
      return Object.assign({}, item, { id: 'id' });
    })

    const parsedItems = parsedJson.items.map(item => {
      return Object.assign({}, item, { id: 'id' });
    })

    const expectedResult = Object.assign({}, expectedJson, { items: expectedItems })
    const parsedResult = Object.assign({}, parsedJson, { items: parsedItems })

    // Assert
    expect(parsedResult).toEqual(expectedResult);
  })
});